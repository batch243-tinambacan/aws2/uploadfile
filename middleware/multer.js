
const multer = require("multer");

// ----- configure how the files are stored ----- //
const storageItem = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    },
    destination: function (req, file, cb) {
        // Check if the file name contains "cert"
        if (file.originalname.toLowerCase().includes("cert")) {
            cb(null, "uploads/certificates");
        } else if (file.originalname.toLowerCase().includes("resume")) {
            cb(null, "uploads/resume");
        } else {
            // If the file name doesn't contain "cert" or "resume", you can set a default destination
            cb(null, "uploads/others");
        }
    }, 
});


// ----- Limit file upload to pdf type only ----- //
const fileFilter = (req, file, cb) => {
    if (
        file.mimetype === "application/pdf"
    ) {
        cb(null, true);
    } else {
            cb(null, false);
        }
};

const uploadItem = multer({
    storage: storageItem,
    limits: {
        fileSize: 1024 * 1024,
    },
    fileFilter: fileFilter,
});

module.exports = uploadItem;


