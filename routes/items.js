const express = require("express");
const router = express.Router();

const uploadItem  = require("../middleware/multer");
const { getItem, addItem} = require("../controllers/items");

router.route("/").get(getItem).post(uploadItem.single("file"), addItem);

module.exports = router;